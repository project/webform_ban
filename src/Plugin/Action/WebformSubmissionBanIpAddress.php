<?php

namespace Drupal\webform_ban\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Ban IP address of a webform submission.
 *
 * @Action(
 *   id = "webform_submission_ban_ip_address",
 *   label = @Translation("Ban IP Address"),
 *   type = "webform_submission"
 * )
 */
class WebformSubmissionBanIpAddress extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /** @var \Drupal\ban\BanIpManager $ban_ip_manager */
    $ban_ip_manager = \Drupal::service('ban.ip_manager');
    if (!$ban_ip_manager->isBanned($entity->getRemoteAddr())) {
      /** @var \Drupal\webform\WebformSubmissionInterface $entity */
      $ban_ip_manager->banIp($entity->getRemoteAddr());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $account->hasPermission('ban IP addresses');
  }

}
