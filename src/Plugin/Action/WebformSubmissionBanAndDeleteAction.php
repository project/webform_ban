<?php

namespace Drupal\webform_ban\Plugin\Action;

use Drupal\webform\Plugin\Action\WebformSubmissionDeleteAction;

/**
 * Redirects to a webform submission deletion form.
 *
 * @Action(
 *   id = "webform_submission_ban_and_delete_action",
 *   label = @Translation("Ban IP and Delete submission"),
 *   type = "webform_submission",
 *   confirm_form_route_name = "webform_submission.multiple_delete_confirm"
 * )
 */
class WebformSubmissionBanAndDeleteAction extends WebformSubmissionDeleteAction {
  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {

    /** @var \Drupal\ban\BanIpManager $ban_ip_manager */
    $ban_ip_manager = \Drupal::service('ban.ip_manager');

    foreach ($entities as $entity) {
      /** @var \Drupal\webform\WebformSubmissionInterface $entity */
      if (!$ban_ip_manager->isBanned($entity->getRemoteAddr())) {
        $ban_ip_manager->banIp($entity->getRemoteAddr());
      }
    }

    parent::executeMultiple($entities);
  }
}
